##  Install tools

su -
apt update
apt dist-upgrade
apt install autoconf automake autotools-dev \
build-essential debhelper devscripts dh-make fakeroot \
lintian pbuilder xutils rpm ruby-dev git \
wget curl ntp links haveged facter sudo 
exit

### Setup configure

* cd helloworld-0.0.1
* autoscan 
* mv configure.scan configure.ac
* edit configure.ac,
  fix AC_INIT
  add AM_INIT_AUTOMAKE 
* aclocal
* autoconf
* autoreconf --install
* automake --add-missing


##  test make file

* ./configure
* make
* make install


http://mij.oltrelinux.com/devel/autoconf-automake/
https://robots.thoughtbot.com/the-magic-behind-configure-make-make-install


##  Build a debian package

su -
apt install build-essential autoconf automake \
autotools-dev dh-make debhelper devscripts fakeroot \
xutils lintian pbuilder
exit

debuild -b -uc -us

debuild -b -uc -us -tc

DEB_BUILD_OPTIONS='nostrip noopt debug' dpkg-buildpackage -b -uc -us



##  References

https://wiki.debian.org/BuildingTutorial

https://wiki.debian.org/HowToPackageForDebian

https://www.debian.org/doc/manuals/maint-guide/first.en.html

https://coderwall.com/p/urkybq/how-to-create-debian-package-from-source

https://www.debian.org/doc/manuals/debmake-doc/ch04.en.html

https://www.debian.org/doc/manuals/maint-guide/dreq.en.html#rules

https://blog.packagecloud.io/eng/2014/10/28/howto-gpg-sign-verify-deb-packages-apt-repositories/

http://blog.jonliv.es/blog/2011/04/26/creating-your-own-signed-apt-repository-and-debian-packages/

